#!groovy
package Jenkinsfiles

pipeline {
    agent any
    stages {
        stage('Clone Stage'){
            steps {
                // withCredentials([usernamePassword(credentialsId: 'c81057ba-df9b-4657-ae8f-2efdae68d743', usernameVariable: 'juanvidher@gmail.com', passwordVariable: 'Juanjose14')]) {
                deleteDir()
                bat 'git clone https://JuanVidHer@bitbucket.org/JuanVidHer/prueba.git'
            }
        }
        stage('Cleaning Stage'){
            steps {
                bat 'cd C:\\Users\\juanjose.vidal\\.jenkins\\workspace\\Pipeline_prueba_local\\prueba && gradlew clean'
            }
        }
        stage('Building Stage'){
            steps {
                bat 'cd C:\\Users\\juanjose.vidal\\.jenkins\\workspace\\Pipeline_prueba_local\\prueba && gradlew build'
            }
        }
        stage('Testing Stage'){
            steps {
                bat 'cd C:\\Users\\juanjose.vidal\\.jenkins\\workspace\\Pipeline_prueba_local\\prueba && gradlew test'
            }
        }

        stage('Reporting Stage') {
            steps {
                bat 'cd C:\\Users\\juanjose.vidal\\.jenkins\\workspace\\Pipeline_prueba_local\\prueba && gradlew aggregate'
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'prueba\\target\\site\\serenity', reportFiles: 'index.html', reportName: ' Serenity Report', reportTitles: ''])
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'prueba\\target\\cucumber', reportFiles: 'index.html', reportName: 'Cucumber Report', reportTitles: ''])
            }
        }
        stage('Sending email') {
            steps {
                emailext  body: '$BUILD_URL', subject: 'build: $BUILD_NUMBER estado: $BUILD_STATUS', to: 'juanjose.vidal@alten.es'

            }
        }
    }
}