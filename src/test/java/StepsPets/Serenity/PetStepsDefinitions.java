package StepsPets.Serenity;



import Support.ServiceSupport;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;
import org.json.JSONObject;
import org.junit.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.ResourceBundle;

import static java.lang.String.format;
import static net.serenitybdd.rest.RestRequests.given;
import static net.serenitybdd.rest.SerenityRest.rest;

import static org.hamcrest.Matchers.equalTo;

public class PetStepsDefinitions {

    private ResourceBundle config = ResourceBundle.getBundle("config/config");
    private ServiceSupport serviceSupport = new ServiceSupport();
    private final String endpoint = config.getString("URI");
    private RequestSpecification specification = rest().baseUri(endpoint);
    private String path;
    private String address= "https://petstore.swagger.io/v2/pet/4";

    /*
    GET
     */

   @Step
    public void getUserByID (int id){
       given().when().get(address).then().body("id",equalTo(id)).assertThat();
    }

    /*@Step
    public void verifyStatusCode (int expectedCode){
        given().when().get (address).then().statusCode(expectedCode).assertThat();
    }
    */
    @Step
    public void verifyParameterValue (String parameter, String parameterValue){
        given().when().get(address).then().body(parameter, equalTo(parameterValue)).assertThat();
        }

     /*
    POST
     */

    @Step("When I request to do a post|put operation with \"<field>\"  and \"<fieldValue>\"")

    public void setHeader(String method,String field , String fieldValue){

        Serenity.setSessionVariable("method").to(method);
        switch (field) {
            case "Content-Type":
                specification.when().contentType(fieldValue);
                if (fieldValue.equals("application/xml"))
                    specification.accept(fieldValue);
                break;
            default:
                break;
        }
    }
    @Step ("with URI \"<string>\"")
    public void setPath (String path){

        this.path=path;
    }

    @Step("And with Body \"/Requests/(put|post)_pet_<expectedStatusCode>.(json|xml)\"")
    public void withBody(String jsonBodyFile) {

        try {
            InputStream is = this.getClass().getResourceAsStream(jsonBodyFile);
            String body = serviceSupport.jsonInputStreamToJsonObject(is);
//            values.forEach((key, val) ->
//                    body.put(key, val)
//            );

            specification = specification.body(body);
            Response response = serviceSupport.executeRequest(specification, Serenity.sessionVariableCalled("method"), endpoint + path);
            Serenity.setSessionVariable("response").to(response);
//            Serenity.setSessionVariable("body").to(body);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Step("Then I should get expected code \"<expectedStatusCode>\"")
    public void verifyStatusCode(int expectedStatusCode) {
        Response res = Serenity.sessionVariableCalled("response");
        Assert.assertEquals("status code doesn't match", expectedStatusCode, res.getStatusCode());
    }
    @Step("And response body equals to \"request/expected_(post|put)_pet_<expectedStatusCode>\"")
    public void verifyBody (String expectedJsonBodyFile){
        try {
            Response res = Serenity.sessionVariableCalled("rsponse");
            InputStream is = this.getClass().getResourceAsStream(expectedJsonBodyFile);
            String body = serviceSupport.jsonInputStreamToJsonObject(is);
            //Assert.assertEquals("The body doesn't match" , format(res.getBody().print()), format(body));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Step("And delete pet with Id equals to")
    public void deleteByID(String id){
        Response response = serviceSupport.executeRequest(specification, Serenity.sessionVariableCalled("method"), endpoint + path + String.format("/%s", id));
        Serenity.setSessionVariable("response").to(response);
    }

}
