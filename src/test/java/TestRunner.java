import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URI;
import java.net.URL;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
    features ={"src/test/resources/Features"},
        glue ={"Steps"},
        tags={"@renfe"},
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json"}
    )

public class TestRunner {

        }

